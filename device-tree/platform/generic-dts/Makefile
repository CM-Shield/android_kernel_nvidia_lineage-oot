# SPDX-FileCopyrightText: Copyright (c) 2022-2023 NVIDIA CORPORATION & AFFILIATES. All rights reserved.
# SPDX-License-Identifier: GPL-2.0-only

objtree ?= $(srctree)

# Redefine the fixdep command
cmd_and_fixdep =                                                             \
	$(cmd);                                                              \
	$(objtree)/scripts/basic/fixdep $(depfile) $@ '$(make-cmd)' > $(dot-target).cmd;\
	rm -f $(depfile)

ifeq ($(origin oottree), undefined)
oottree := $(srctree.nvidia-oot)
endif
include $(oottree)/scripts/Makefile.lib

DTC_CPP_FLAGS += $(EXTRA_CPP_FLAGS)

DTC_CPP_FLAGS += -DLINUX_VERSION=600 -DTEGRA_HOST1X_DT_VERSION=2

ifneq ($(findstring ack_src,$(NV_BUILD_KERNEL_OPTIONS)),)
ifneq ($(TOP),)
SOURCE_TOP := $(TOP)
else
$(error TOP is not defined)
endif
else
ifneq ($(TEGRA_TOP),)
SOURCE_TOP := $(TEGRA_TOP)
else ifneq ($(TOP),)
SOURCE_TOP := $(TOP)
else
$(error TEGRA_TOP or TOP is not defined)
endif
endif

tegra-dtstree := $(SOURCE_TOP)/hardware/nvidia

null :=
space :=$(null) $(null)

DTC_INCLUDE :=
# SOC independent common include
DTC_INCLUDE += $(tegra-dtstree)/tegra/nv-public

ifeq ($(SRCARCH),arm64)
ifeq ($(CONFIG_ARCH_TEGRA_234_SOC),y)
# SOC T23X specific common include
DTC_INCLUDE += $(tegra-dtstree)/t23x/nv-public/include/kernel
DTC_INCLUDE += $(tegra-dtstree)/t23x/nv-public/include/nvidia-oot
DTC_INCLUDE += $(tegra-dtstree)/t23x/nv-public/include/platforms
DTC_INCLUDE += $(tegra-dtstree)/t23x/nv-public
endif
DTC_INCLUDE += $(srctree)/arch/arm64/boot/dts/nvidia
else ifeq ($(SRCARCH),arm)
LINUX_VERSION     := $(shell expr $(VERSION) \* 256 + $(PATCHLEVEL))
LINUX_VERSION_6_5 := $(shell expr 6 \* 256 + 5)
ifeq ($(shell test $(LINUX_VERSION) -ge $(LINUX_VERSION_6_5); echo $$?),0)
DTC_INCLUDE += $(srctree)/arch/arm/boot/dts/nvidia
else
DTC_INCLUDE += $(srctree)/arch/arm/boot/dts
endif
endif

dtb-y :=
dtbo-y :=
fdts :=
ifeq ($(CONFIG_ARCH_TEGRA_114_SOC),y)
fdts += $(tegra-dtstree)/t114
endif
ifeq ($(CONFIG_ARCH_TEGRA_124_SOC),y)
fdts += $(tegra-dtstree)/t124
endif
ifeq ($(CONFIG_ARCH_TEGRA_210_SOC),y)
fdts += $(tegra-dtstree)/t21x
endif
ifeq ($(CONFIG_ARCH_TEGRA_186_SOC),y)
fdts += $(tegra-dtstree)/t18x
endif
ifeq ($(CONFIG_ARCH_TEGRA_194_SOC),y)
fdts += $(tegra-dtstree)/t19x
endif
ifeq ($(CONFIG_ARCH_TEGRA_234_SOC),y)
fdts += $(tegra-dtstree)/t23x
endif

# Add internal SOCs to scan the DT makefiles
ifneq ($(internal_soc_list),)
fdts += $(addprefix $(tegra-dtstree)/,$(internal_soc_list))
endif

# Remove the DTs from protected soc list
ifneq ($(protected_soc_list),)
kdts := $(foreach dt_path, $(fdts), $(if $(filter $(protected_soc_list),$(patsubst -,$(space),$(subst /, $(space),$(dt_path)))),,$(dt_path)))
kdts := $(filter-out $(space)$(space),$(kdts))
else
kdts := $(fdts)
endif

dts_makefile=$(foreach d,$(wildcard $1*), $(call dts_makefile,$(d)/,$(2)) $(if $(findstring Makefile,$(d)),$(d)))
dts_mfiles = $(call dts_makefile, $(kdts), Makefile)

ifneq ($(dts_mfiles),)
dts-include :=
include $(dts_mfiles)
ifneq ($(dts-include),)
DTC_INCLUDE += $(addprefix $(tegra-dtstree)/,$(dts-include))
endif
endif

# Add path of main Makefile to each dtb/dtbo list
DTB_OBJS := $(addprefix $(obj)/,$(dtb-y))
DTBO_OBJS := $(addprefix $(obj)/,$(dtbo-y))

###
# remove the ../
replace_ddot = $(subst ../,,$(1))

define _define_dtb_rule
$(obj)/$(1): $(tegra-dtstree)/$(patsubst %.dtb,%.dts,$(1)) FORCE
endef

$(foreach _dtb, $(dtb-y), $(eval $(call _define_dtb_rule,$(_dtb))))

$(DTB_OBJS):
	$(call if_changed_dep,dtc)

define _define_dtbo_rule
$(obj)/$(1): $(tegra-dtstree)/$(patsubst %.dtbo,%.dts,$(1)) FORCE
endef

$(foreach _dtbo, $(dtbo-y), $(eval $(call _define_dtbo_rule,$(_dtbo))))

$(DTBO_OBJS):
	$(call if_changed_dep,dtc)

DTBS_DTBOS := $(DTB_OBJS)$(DTBO_OBJS)

dtbs: $(DTB_OBJS) $(DTBO_OBJS) FORCE
	if [ ! -d $(obj)/dtbs/ ] ; then \
		mkdir -p $(obj)/dtbs/ ; \
	fi
	if [ ! -z "$(DTBS_DTBOS)" ] ; then \
		cp -u $(DTB_OBJS) $(DTBO_OBJS) $(obj)/dtbs/ ; \
	fi
	if [ -d $(obj)/hardware/ ] ; then \
		rm -rf $(obj)/hardware/ ; \
	fi

clean-files := *.dtb *.dtbo *.tmp
